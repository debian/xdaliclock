/* Dali Clock - a melting digital clock for Palm WebOS.
 * Copyright (c) 1991-2009 Jamie Zawinski <jwz@jwz.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 */

function PrefsAssistant(model, hide_cb) {
  this.model   = model;
  this.hide_cb = hide_cb;
}


PrefsAssistant.prototype.setup = function() {

  // Create the option menus
  //
  this.controller.setupWidget('timeModeSelector', {
      label:         'Time Display',
      modelProperty: 'time_mode',
      choices: [
       {label: 'Hour, Min & Sec', value: 'HHMMSS' },
       {label: 'Hour & Min Only', value: 'HHMM'   },
       {label: 'Seconds Only',    value: 'SS'     },
       ]},
    this.model);

  this.controller.setupWidget('dateModeSelector', {
      label:         'Date Display',
      modelProperty: 'date_mode',
      choices: [
       {label: 'Month/Day/Year', value: 'MMDDYY' },
       {label: 'Day/Month/Year', value: 'DDMMYY' },
       {label: 'Year/Month/Day', value: 'YYMMDD' },
       ]},
    this.model);

  this.controller.setupWidget('twelveHourRadio', {
      modelProperty: 'twelve_hour_p',
      choices: [
       {label: '12 Hour Time', value: true  },
       {label: '24 Hour Time', value: false },
       ]},
    this.model);

  this.controller.setupWidget('fpsSlider', {
      modelProperty: 'fps',
      minValue: '1',
      maxValue: '30',
     },
    this.model);

  this.controller.setupWidget('cpsSlider', {
      modelProperty: 'cps',
      minValue: '0',
      maxValue: '30',
     },
    this.model);

}


PrefsAssistant.prototype.deactivate = function(event) {
  if (this.hide_cb)
    this.hide_cb.call();  // callback from parent
}
