/* Dali Clock - a melting digital clock for Android.
 * Copyright (c) 1991-2015 Jamie Zawinski <jwz@jwz.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 *
 * Ported to Android 2015 by Robin Müller-Cajar <robinmc@mailbox.org>
 */

package org.jwz.daliclock;

import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.service.dreams.DreamService;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.widget.LinearLayout;

public class DaliDreamService extends DreamService {

  DaliClock clock;
  Display display;
  Handler date_timer;

  public DaliDreamService() {
  }

  @Override
  public void onAttachedToWindow() {
    super.onAttachedToWindow();

    setInteractive (false);
    setFullscreen (true);

    // DaliClock/app/src/main/res/layout/dream_layout.xml
    setContentView (R.layout.dream_layout);

    display = getWindowManager().getDefaultDisplay();
    LinearLayout bdiv = (LinearLayout) findViewById(R.id.clockbg);
    SurfaceView canvas = (SurfaceView) findViewById(R.id.canvas);
    SharedPreferences settings = 
      PreferenceManager.getDefaultSharedPreferences(this);
    SharedPreferences.Editor editor = settings.edit();

    editor.putBoolean("show_date_p", false);

    Point size = new Point();
    display.getRealSize(size); // using realSize as we are in full screen

    editor.putInt ("width",  (int) size.x);
    editor.putInt ("height", (int) size.y);

    editor.apply();

    if (clock != null) {
      clock.hide();
      clock.changeSettings(settings);
    } else {
      clock = new DaliClock(this);
      clock.setup(canvas, bdiv, settings);
    }
    clock.show();
    startDateTimer();
  }


  // Display the date every minute-and-a-bit.  That way it doesn't show up
  // on the same second each minute.
  //
  private void startDateTimer() {
    float display_secs = 67;

    if (date_timer == null)
      date_timer = new Handler();

    date_timer.removeCallbacksAndMessages(null);

    Runnable runnable = new Runnable() {
        @Override
        public void run() { dateTimer(); }
      };
    date_timer.postDelayed (runnable, (int) (display_secs * 1000));
  }


  private void dateTimer() {
    SharedPreferences settings = 
      PreferenceManager.getDefaultSharedPreferences(this);
    SharedPreferences.Editor editor = settings.edit();
    editor.putBoolean("show_date_p", true);
    editor.apply();
    clock.changeSettings (settings);
    startDateTimer();
  }


/*
  public void onDreamingStarted() {
    super.onDreamingStarted();
    clock.show();
  }
*/

  public void onDreamingStopped() {
    super.onDreamingStopped();
    clock.hide();
  }

  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    clock.cleanup();
    if (date_timer != null) {
      date_timer.removeCallbacksAndMessages(null);
      date_timer = null;
    }
  }

}
